import pathlib
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox, filedialog
import pygubu

from models import FileBackend

PROJECT_PATH = pathlib.Path(__file__).parent
PROJECT_UI = PROJECT_PATH / "ste.ui"


class SimpleTextEditorApp:
    def __init__(self, master=None):
        self.builder = builder = pygubu.Builder()
        builder.add_resource_path(PROJECT_PATH)
        builder.add_from_file(PROJECT_UI)
        self.mainwindow = builder.get_object('root', master)
        self.text = builder.get_object('text', master)
        builder.connect_callbacks(self)
        self.model = FileBackend(self.refresh_content)
        self.protected = False
        self.update_title()
        self.text.focus()
    
    def run(self):
        self.mainwindow.mainloop()

    def refresh_content(self):
        if not self.protected:
            self.text.delete("1.0", tk.END)
            self.text.insert(tk.END, self.model.content)
    
    def update_title(self):
        is_dirty = "* " if self.model.dirty else ""
        filename = self.model.filename or "New File"
        self.mainwindow.title(f"{is_dirty}{filename} | Simple Text Editor")

    def scrollbar_set(self, first, last):
        self.builder.get_object('scrollbar').set(first, last)

    def open_context_menu(self, event=None):
        edit_menu = self.builder.get_object('edit')
        try:
            edit_menu.tk_popup(event.x_root, event.y_root)
        finally:
            edit_menu.grab_release()

    def keystroke(self, event=None):
        content = self.text.get("1.0", tk.END)
        if content[-1] == '\n':
            content = content[:-1]  # why does Text append a new line??
        self.protected = True
        self.model.content = content
        self.protected = False
        self.update_title()

    def text_yview(self, mode=None, value=None, units=None):
        self.text.yview(mode, value, units)

    def cmd_open(self, _ = None):
        if self.model.dirty:
            if not messagebox.askyesno(
                    title="Open File",
                    message="Do you really want to open a file? Unsaved "
                    "changes will be lost."):
                return
        filename = filedialog.askopenfilename()
        if filename == () or filename == '':
            return
        self.model.open(filename)

    def cmd_new(self, _ = None):
        if self.model.dirty:
            if not messagebox.askyesno(
                    title="New File",
                    message="Do you really want to create a new file? Unsaved "
                    "changes will be lost."):
                return
        self.model.new()

    def cmd_save(self, _ = None):
        if not self.model.filename:
            return self.cmd_save_as()
        self.model.save(self.model.filename)

    def cmd_save_as(self, _ = None):
        filename = filedialog.asksaveasfilename()
        if filename == () or filename == '':
            return
        self.model.save(filename)

    def cmd_quit(self, _ = None):
        dirty = ""
        if self.model.dirty:
            dirty = " Unsaved changes will be lost"
        if not messagebox.askyesno(
                title="Quit",
                message=f"Do you really want to quit?{dirty}"):
            return
        self.mainwindow.destroy()

    def cmd_undo(self, _ = None):
        self.text.event_generate('<<Undo>>')
        self.keystroke()

    def cmd_redo(self, _ = None):
        self.text.event_generate('<<Redo>>')
        self.keystroke()

    def cmd_cut(self, _ = None):
        self.text.event_generate('<<Cut>>')
        self.keystroke()

    def cmd_copy(self, _ = None):
        self.text.event_generate('<<Copy>>')
        self.keystroke()

    def cmd_paste(self, _ = None):
        self.text.event_generate('<<Paste>>')
        self.keystroke()


if __name__ == '__main__':
    app = SimpleTextEditorApp()
    app.run()


