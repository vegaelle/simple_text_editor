from hashlib import sha256
from typing import Optional, Callable


class FileBackend:


    def __init__(self, on_update: Callable):
        self.dirty: bool = False
        self.filename: Optional[str] = None
        self._content: str = ''
        self.on_update = on_update
        self.refresh_hash()

    def refresh_hash(self):
        self.hash = sha256(self.content.encode())

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, new_value):
        # if new_value[-1] == '\n':
        #     new_value = new_value[:-1]
        # print(f'new value: <{new_value}> ({repr(new_value[-1])})')
        self._content = new_value
        new_hash = sha256(self.content.encode())
        if new_hash.digest() != self.hash.digest():
            self.dirty = True
        else:
            self.dirty = False
        self.on_update()

    def new(self):
        self.dirty = False
        self.filename = None
        self.content = ''
        self.refresh_hash()

    def open(self, filename: str):
        with open(filename) as fd:
            self.content = fd.read()
        self.filename = filename
        self.dirty = False
        self.refresh_hash()

    def save(self, filename: str):
        with open(filename, mode='w') as fd:
            fd.write(self.content)
        self.dirty = False
        self.filename = filename
        self.refresh_hash()


