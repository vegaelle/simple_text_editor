import tkinter as tk

from utils import action
from widgets import AppMenu, EditorWidget
from models import FileBackend


class TextEditor(tk.Tk):

    def __init__(self, theme: str=None):
        super().__init__()
        if theme:
            theme_file = theme
            theme_name = None
            if ':' in theme:
                theme_file, theme_name = theme.split(':')
            self.tk.call("source", theme_file)
            if theme_name:
                self.tk.call("set_theme", theme_name)
            
        self.build()
        self.model = FileBackend(
            on_update=self.editor.refresh_content
        )
        self.geometry()
        self.bindings()
    
    def __call__(self):
        self.mainloop()


    def build(self):
        self.menubar = AppMenu(self,
            {
                "file": [
                        self.new_file, self.open_file, self.save_file,
                        self.save_file_as, '', self.quit
                    ],
                "edit": [
                    self.undo, self.redo, '', self.cut, self.copy, self.paste
                ]
            }
        )
        self.editor = EditorWidget(self)

    def geometry(self):
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.menubar.geometry()
        self.editor.geometry()
        self.update_title()

    def bindings(self):
        self.menubar.bindings()
        self.editor.bindings()

    def update_title(self):
        is_dirty = "* " if self.model.dirty else ""
        filename = self.model.filename or "New File"
        self.title(f"{is_dirty}{filename} | Simple Text Editor")

    def open_context_menu(self, event):
        try:
            self.menubar.edit_menu.tk_popup(event.x_root, event.y_root)
        finally:
            self.menubar.edit_menu.grab_release()

    def keystroke(self, _):
        content = self.editor.text.get("1.0", tk.END)
        if content[-1] == '\n':
            content = content[:-1]  # why does Text append a new line??
        self.editor.protected = True
        self.model.content = content
        self.editor.protected = False
        self.update_title()

    @action(shortcut="Control-n")
    def new_file(self):
        if self.model.dirty:
            if not messagebox.askyesno(
                    title="New File",
                    message="Do you really want to create a new file? Unsaved "
                    "changes will be lost."):
                return
        self.model.new()

    @action(shortcut="Control-o")
    def open_file(self):
        if self.model.dirty:
            if not messagebox.askyesno(
                    title="Open File",
                    message="Do you really want to open a file? Unsaved "
                    "changes will be lost."):
                return
        filename = filedialog.askopenfilename()
        if filename == () or filename == '':
            return
        self.model.open(filename)
    
    @action(shortcut="Control-s")
    def save_file(self):
        if not self.model.filename:
            return self.save_file_as()
        self.model.save(self.model.filename)

    @action(shortcut="Control-S")
    def save_file_as(self):
        filename = filedialog.asksaveasfilename()
        if filename == () or filename == '':
            return
        self.model.save(filename)

    @action(shortcut="Control-q")
    def quit(self):
        dirty = ""
        if self.model.dirty:
            dirty = " Unsaved changes will be lost"
        if not messagebox.askyesno(
                title="Quit",
                message=f"Do you really want to quit?{dirty}"):
            return
        self.destroy()

    @action(shortcut="Control-z")
    def undo(self):
        self.editor.text.event_generate('<<Undo>>')
        self.keystroke(None)

    @action(shortcut="Control-Z")
    def redo(self):
        self.editor.text.event_generate('<<Redo>>')
        self.keystroke(None)

    @action(shortcut="Control-x")
    def cut(self):
        self.editor.text.event_generate('<<Cut>>')
        self.keystroke(None)

    @action(shortcut="Control-c")
    def copy(self):
        self.editor.text.event_generate('<<Copy>>')
        self.keystroke(None)

    @action(shortcut="Control-v")
    def paste(self):
        self.editor.text.event_generate('<<Paste>>')
        self.keystroke(None)
