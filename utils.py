from functools import wraps


def action(label: str=None, shortcut: str=None):

    def decorator(meth):
        if label:
            meth.label = label
        
        if shortcut:
            meth.shortcut = shortcut
            
        return meth

    return decorator


def binding_to_accelerator(binding: str) -> str:
    mods = {'Control': 'Ctrl'}
    
    components = binding.split('-')
    *modifiers, key = components
    if key.isupper():
        modifiers.append('Shift')
    modifiers = [mods.get(mod, mod).title() for mod in modifiers]
    key = key.title()
    return '+'.join(modifiers + [key])


def to_listener(func):

    @wraps(func)
    def listener(_, *args, **kwargs):
        return func(*args, **kwargs)

    return listener

