import tkinter as tk
from tkinter import ttk
import typing

from utils import binding_to_accelerator, to_listener


if typing.TYPE_CHECKING:
    from editor import TextEditor

class AppMenu(tk.Menu):


    def __init__(self, root: "TextEditor", contents: dict):
        super().__init__(root)
        self.contents = contents
        self.create_menu()
    
    def bindings(self):
        for menu_contents in self.contents.values():
            for action in menu_contents:
                if hasattr(action, 'shortcut'):
                    self.bind_all(f"<{action.shortcut}>", to_listener(action))
    
    def create_menu(self):
        for label, menu_contents in self.contents.items():
            menu = tk.Menu(self, tearoff=0)
            self.add_cascade(label=label.title(), menu=menu)
            setattr(self, f"{label}_menu", menu)
            for action in menu_contents:
                if action == '':
                    menu.add_separator()
                else:
                    args = {"command": action}
                    if hasattr(action, "label"):
                        args['label'] = action.label
                    else:
                        args['label'] = action.__name__.replace('_', ' ').title()

                    if hasattr(action, "shortcut"):
                        args['accelerator'] = binding_to_accelerator(action.shortcut)
                    menu.add_command(**args)

    def geometry(self):
        self.master.config(menu=self)


class EditorWidget(ttk.Frame):

    def __init__(self, root: "TextEditor"):
        super().__init__(root)
        self.text = tk.Text(self, undo=True)
        self.scrollbar = tk.Scrollbar(self, command=self.text.yview)
        self.text["yscrollcommand"] = self.scrollbar.set
        self.text.focus()
        self.protected: bool = False

    def geometry(self):
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.grid(row=0, column=0, sticky="nsew")
        self.text.grid(row=0, column=0, sticky="nsew")
        self.scrollbar.grid(row=0, column=1, sticky="ns")

    def bindings(self):
        self.bind("<KeyRelease>", self.master.keystroke)
        self.bind("<Button-3>", self.master.open_context_menu)

    def refresh_content(self):
        if not self.protected:
            self.text.delete("1.0", tk.END)
            self.text.insert(tk.END, self.master.model.content)


