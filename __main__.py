#!/usr/bin/env python3

import sys

from editor import TextEditor


if __name__ == '__main__':
    if len(sys.argv) == 2:
        app = TextEditor(sys.argv[1])
    else:
        app = TextEditor()
    app()
